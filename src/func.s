	.text
	.globl	func
	.align	4
	.type	func,@function
	.cc_top func.function,func
func:
	entsp 2
	mov r1, r0
	stw r0, sp[1]
	add r0, r0, 1
	stw r1, sp[0]
	retsp 2
	# RETURN_REG_HOLDER
	.cc_bottom func.function
	.set	func.nstackwords,2
	.globl	func.nstackwords
	.set	func.maxcores,1
	.globl	func.maxcores
	.set	func.maxtimers,0
	.globl	func.maxtimers
	.set	func.maxchanends,0
	.globl	func.maxchanends
.Ltmp0:
	.size	func, .Ltmp0-func


	.typestring func, "f{si}(si)"
	.section	.trap_info,"",@progbits
.Ltrap_info_entries_start0:
	.long	.Ltrap_info_entries_end0-.Ltrap_info_entries_start0
	.long	1
.Ltrap_info_entries_end0:
